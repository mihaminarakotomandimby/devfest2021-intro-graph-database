<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;


class MySQLController extends AbstractController
{
    private $pdo;
    public function friendsOf(string $username): Response
    {
        $this->pdo = new \PDO('mysql:host=localhost;'
                              .'port=3306;'
                              .'dbname=devfest;',
                              'root',
                              'root');
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $stmt = $this->pdo->prepare("SELECT u1.name, u2.name 
                                     FROM friendship 
                                     JOIN user u1 ON friendship.user1 = u1.id 
                                     JOIN user u2 ON friendship.user2 = u2.id 
                                     HAVING u1.name = :username");
        $stmt->execute([':username' => $username]);
        $result=[];
        if ($stmt->rowCount() > 0){
            while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
                $result[]=$row;
            }
        } else {
            $stmt = $this->pdo->prepare("SELECT u2.name, u1.name 
                                     FROM friendship 
                                     JOIN user u1 ON friendship.user1 = u1.id 
                                     JOIN user u2 ON friendship.user2 = u2.id 
                                     HAVING u2.name = :username");
            $stmt->execute([':username' => $username]);
            while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
                $result[]=$row;
            }
        }
        return $this->json($result);
    }
}
