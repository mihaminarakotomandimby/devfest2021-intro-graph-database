<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Laudis\Neo4j\ClientBuilder;

class Neo4jController extends AbstractController
{
    private $client;
    private function init(): void
    {
        $this->client = ClientBuilder::create()
                      ->withDriver('bolt', 'bolt://neo4j:devfest@localhost') 
                      ->withDefaultDriver('bolt')
                      ->build();
    }

    public function friendsOf(string $username): Response
    {
        $this->init();
        $results = $this->client->run('MATCH (p:Person)-[FRIEND]-(f:Person {name : "'.$username.'"}) RETURN p.name');
        return $this->json($results);
    }

    public function recommandationsFor(string $username): Response
    {
        $this->init();
        $results = $this->client->run('MATCH (p:Person {name : "'.$username.'"})-[FRIEND]-(f:Person)-[STUD]-(i:Institute) RETURN f.name,i.name');
        return $this->json($results);
    }
    
}
